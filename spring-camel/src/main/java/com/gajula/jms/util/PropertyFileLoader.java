package com.gajula.jms.util;

import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;

public class PropertyFileLoader implements Serializable{

	private static final long serialVersionUID = -372854405689117321L;
	private static PropertyFileLoader singleton;
	private transient ResourceBundle rb = null;
	
	private PropertyFileLoader(){
		System.out.println("Default");
		load();
	}
	
	public void load(){
		rb = ResourceBundle.getBundle("com/gajula/jms/util/myProp",Locale.getDefault());
	}
	
	public static PropertyFileLoader getInstance(){
		System.out.println("getInstance");
		singleton = new PropertyFileLoader();
		return singleton;
	}
	
	public String getProperty(String key){
		if (rb.containsKey(key)){
			System.out.println("Get Value:::"+rb.getString(key));
			return rb.getString(key);
		}else{
			return "Property not found for key " + key;
		}	
	}
	
}
