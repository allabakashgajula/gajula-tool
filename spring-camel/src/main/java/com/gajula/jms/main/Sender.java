package com.gajula.jms.main;

import java.io.File;

import javax.jms.Queue;

import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.BlobMessage;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gajula.jms.service.JmsMessageSender;
 
 
public class Sender {
 
  public static void main(String[] args) {
    
	  
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    JmsMessageSender jmsMessageSender = (JmsMessageSender)ctx.getBean("jmsMessageSender");
    jmsMessageSender.send("hello JMS");
    
    Queue queue1 = new ActiveMQQueue("TO-2");
    jmsMessageSender.sendPMFile(queue1, "PM");
    jmsMessageSender.sendPCAFile(queue1, "PCA");
    jmsMessageSender.sendDNSFile(queue1, "DNS");
    ((ClassPathXmlApplicationContext)ctx).close();
  }
 
}
