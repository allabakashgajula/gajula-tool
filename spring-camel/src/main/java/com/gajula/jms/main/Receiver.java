package com.gajula.jms.main;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gajula.jms.service.JmsMessageReceiver;
import com.gajula.jms.service.JmsMessageSender;

public class Receiver {

	public static void main(String[] args) {
	    
		  
	    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
	    JmsMessageReceiver jmsMessageReceiver = (JmsMessageReceiver)ctx.getBean("jmsMessageReceiver");
	    Queue queue = new ActiveMQQueue("TO-2");
	    jmsMessageReceiver.receive(queue);
	    ((ClassPathXmlApplicationContext)ctx).close();
	  }
}
