package com.gajula.jms.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.StreamMessage;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
  
@Service
public class JmsMessageSender {
  
  @Autowired
  private JmsTemplate jmsTemplate;
    
    
  public void send(final String text) {
    this.jmsTemplate.send(new MessageCreator() {
      public Message createMessage(Session session) throws JMSException {
    	  System.out.println("--1111---Send-----------"+text);
        Message message = session.createTextMessage(text);     
        message.setJMSReplyTo(new ActiveMQQueue("ReeiverToSender"));
        return message;
      }
    });
  }
    
  public void sendText(final String text) {
    this.jmsTemplate.convertAndSend(text);
  }
    
 public void sendPMFile(final Destination dest,final String text) {
    this.jmsTemplate.send(dest,new MessageCreator() {
      public Message createMessage(Session session) throws JMSException {
   	  System.out.println("--2222-Send---------"+text);
   	  StreamMessage blobMessage = session.createStreamMessage();
      byte[] array;
	  try{
			array = Files.readAllBytes(new File("C:/GAJULA/AGCO/NAPT/CSV/NAPT_PM.csv").toPath());
			blobMessage.writeBytes(array);
			blobMessage.setStringProperty("serviceType", "pm");
		}catch (IOException e){
			e.printStackTrace();
		}
        return blobMessage;
      }
    });
  }
 
 public void sendPCAFile(final Destination dest,final String text) {
	    this.jmsTemplate.send(dest,new MessageCreator() {
	      public Message createMessage(Session session) throws JMSException {
	   	  System.out.println("--2222-Send---------"+text);
	        StreamMessage blobMessage = session.createStreamMessage();
	        byte[] array;
			try {
				array = Files.readAllBytes(new File("C:/GAJULA/AGCO/NAPT/CSV/NAPT_PCA.csv").toPath());
				blobMessage.writeBytes(array);
				blobMessage.setStringProperty("serviceType", "pca");
			} catch (IOException e) {
				e.printStackTrace();
			}
	        return blobMessage;
	      }
	    });
	  }
 
 	public void sendDNSFile(final Destination dest,final String text) {
	    this.jmsTemplate.send(dest,new MessageCreator() {
	      public Message createMessage(Session session) throws JMSException {
	   	  System.out.println("--2222-Send---------"+text);
	      StreamMessage blobMessage = session.createStreamMessage();
	      byte[] array;
		  try{
				array = Files.readAllBytes(new File("C:/GAJULA/AGCO/NAPT/CSV/NAPT_Define New Supplier.csv").toPath());
				blobMessage.writeBytes(array);
				blobMessage.setStringProperty("serviceType", "dns");
			}catch (IOException e){
				e.printStackTrace();
			}
	        return blobMessage;
	      }
	    });
	  }
 
}