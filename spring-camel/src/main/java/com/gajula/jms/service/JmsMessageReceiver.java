package com.gajula.jms.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.StreamMessage;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
  
@Service
public class JmsMessageReceiver {
  
  @Autowired
  private JmsTemplate jmsTemplate;
    
    
  public void receive(final Destination destination) {
	Message msg = this.jmsTemplate.receive(destination);
    StreamMessage blobMessage = (StreamMessage)msg;
    try{
    	String serviceType = blobMessage.getStringProperty("serviceType");
    	System.out.println("SERVICE::"+serviceType);
		Object obj = blobMessage.readObject();
		if(serviceType.equalsIgnoreCase("pm")){
			FileOutputStream fout = new FileOutputStream("C:/GAJULA/PM.csv");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(obj);
		}else if(serviceType.equalsIgnoreCase("pca")){
			FileOutputStream fout = new FileOutputStream("C:/GAJULA/PCA.csv");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(obj);
		}else if(serviceType.equalsIgnoreCase("dns")){
			FileOutputStream fout = new FileOutputStream("C:/GAJULA/DNS.csv");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(obj);
		}
	}catch(JMSException e){
		e.printStackTrace();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
  }
    
  public void sendText(final String text) {
    this.jmsTemplate.convertAndSend(text);
  }
}