package com.gajula.dao;

import com.gajula.dto.UsersDto;

public interface HomeDao {

	public UsersDto getUsersDetails()throws Exception;
}
