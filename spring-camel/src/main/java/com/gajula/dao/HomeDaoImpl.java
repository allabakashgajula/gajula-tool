package com.gajula.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.gajula.dto.UsersDto;

//@Repository
public class HomeDaoImpl implements HomeDao{

	@Autowired
	SessionFactory sessionFactory;
	
	public UsersDto getUsersDetails() throws Exception {
		Session session = null;
		UsersDto user = new UsersDto();
		try{
			System.out.println("DAO getUsersDetails Start");
			session = sessionFactory.openSession();
			Criteria crit = session.createCriteria(UsersDto.class);
			@SuppressWarnings("unchecked")
			List<UsersDto> list = crit.list();
			System.out.println("Users Size==========="+list.size());
			if(list.size()!=0){
				user = list.get(0);
			}
			System.out.println("DAO getUsersDetails End");
		}catch(Exception e){
			e.printStackTrace();
		}
		return user;
	}

}
