package com.gajula.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.gajula.bean.UsersBean;

@Component
public class UserFormValidator implements Validator{

	public boolean supports(Class<?> clazz) {
		return UsersBean.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		UsersBean userForm = (UsersBean)target;	
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "NotEmpty.userForm.userName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.userForm.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.userForm.lastName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "gender", "NotEmpty.userForm.gender");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobile", "NotEmpty.userForm.mobile");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address1", "NotEmpty.userForm.address1");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address2", "NotEmpty.userForm.address2");
		
		if(userForm.getUserName()==null){
			errors.rejectValue("userName", "NotEmpty.userForm.userName");
			errors.rejectValue("firstName", "NotEmpty.userForm.firstName");
			errors.rejectValue("lastName", "NotEmpty.userForm.lastName");
			errors.rejectValue("gender", "NotEmpty.userForm.gender");
			errors.rejectValue("mobile", "NotEmpty.userForm.mobile");
			errors.rejectValue("address1", "NotEmpty.userForm.address1");
			errors.rejectValue("address2", "NotEmpty.userForm.address2");
		}
	}

}
