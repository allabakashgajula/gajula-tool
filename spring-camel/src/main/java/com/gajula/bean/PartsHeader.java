package com.gajula.bean;

import java.io.Serializable;

public class PartsHeader implements Serializable{
	
	private String partNumber;
	private String factoryCode;
	private String qty;
	private String price;
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getFactoryCode() {
		return factoryCode;
	}
	public void setFactoryCode(String factoryCode) {
		this.factoryCode = factoryCode;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "PartsBean [partNumber=" + partNumber + ", factoryCode="
				+ factoryCode + ", qty=" + qty + ", price=" + price + "]";
	}
}
