package com.gajula.bean;

import java.io.Serializable;
import java.util.List;

public class PartsTemplate implements Serializable{
	
	private PartsHeader partsHeader;
	private List<PartsBean> partsRecords;
	private String errorMessage;
	private String errorMessageFlag;
	private String fileContent;
	
	public PartsHeader getPartsHeader() {
		return partsHeader;
	}
	public void setPartsHeader(PartsHeader partsHeader) {
		this.partsHeader = partsHeader;
	}
	public List<PartsBean> getPartsRecords() {
		return partsRecords;
	}
	public void setPartsRecords(List<PartsBean> partsRecords) {
		this.partsRecords = partsRecords;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getFileContent() {
		return fileContent;
	}
	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}
	public String getErrorMessageFlag() {
		return errorMessageFlag;
	}
	public void setErrorMessageFlag(String errorMessageFlag) {
		this.errorMessageFlag = errorMessageFlag;
	}
	@Override
	public String toString() {
		return "PartsTemplate [partsHeader=" + partsHeader + ", partsRecords="
				+ partsRecords + ", errorMessage=" + errorMessage
				+ ", errorMessageFlag=" + errorMessageFlag + ", fileContent="
				+ fileContent + "]";
	}
}
