package com.gajula.bean;

import java.io.Serializable;
import java.math.BigInteger;

public class UsersBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String userId;
	private BigInteger roleId;
	private String userName;
	private String lastName;
	private String firstName;
	private String gender;
	private String mobile;
	private String address1;
	private String address2;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public BigInteger getRoleId() {
		return roleId;
	}
	public void setRoleId(BigInteger roleId) {
		this.roleId = roleId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	@Override
	public String toString() {
		return "UsersBean [userId=" + userId + ", roleId=" + roleId
				+ ", userName=" + userName + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", gender=" + gender
				+ ", mobile=" + mobile + ", address1=" + address1
				+ ", address2=" + address2 + "]";
	}
	
}
