package com.gajula.processor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FileSharingInfo;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.users.FullAccount;

public class DBXProcessor implements Processor{

	final static String APP_KEY = "djv3fc6evsulrcd";
    final static String APP_SECRET = "69on0v803yo7xve";
    final static String ACCESS_TOKEN = "tQ-9T471PtQAAAAAAAAF07zeNUKG3ztegU01hncGsz67ohgq5RJqa7uJBDFhv2d-";
    final static File inputFile = new File("C:/Users/allabakashg/git/spring-camel_V1.1.zip");
    
    @Override
	public void process(Exchange ex) throws Exception {
		ex.getIn().setBody("<h1>HI</h1>");
		DbxClientV2 client = getDBXConfig();
		getDbxUpload(client,inputFile);
		ex.getOut().setBody(ex);
	}
    
	public DbxClientV2 getDBXConfig()throws Exception{
		DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);
        DbxRequestConfig config = new DbxRequestConfig("dropbox/java", "en_US");
        DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);
        //Get current account info
        FullAccount account = client.users().getCurrentAccount();
        System.out.println(account.getName().getDisplayName());
        return client;
	}
	
	public void getDbxFiles(DbxClientV2 client) throws Exception{
		//Get files and folder metadata from Dropbox root directory
		ListFolderResult result = client.files().listFolder("");
        while(true) {
            for(Metadata metadata : result.getEntries()) {
            	System.out.println(metadata.getPathLower());
            }
            if(!result.getHasMore()){
                break;
            }
            result = client.files().listFolderContinue(result.getCursor());
        }
	}
	
	public void getDbxUpload(DbxClientV2 client, File filePath) throws Exception{ 
        // Upload "test.txt" to Dropbox
        try (InputStream in = new FileInputStream(filePath)) {
        	System.out.println("--STart---");
            FileMetadata metadata = client.files().uploadBuilder("/spring-camel_V1.1.zip").uploadAndFinish(in);;
            FileSharingInfo fsi= metadata.getSharingInfo();
            System.out.println("--End---");
        }
	}
	
}
