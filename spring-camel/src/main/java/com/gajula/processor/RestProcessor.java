package com.gajula.processor;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.restlet.Response;
import org.restlet.ext.servlet.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.gajula.bean.PartsTemplate;
import com.gajula.dto.UsersDto;
import com.gajula.util.CSVUtil;

public class RestProcessor implements Processor{

	@Autowired
	SessionFactory sessionFactory;
	
	
	
	public void process(Exchange exchange) throws Exception {
		System.out.println("process Start"+new Date());
		UsersDto user = new UsersDto();
		Session session = null;
		try{
			session = sessionFactory.openSession();
			Criteria crit = session.createCriteria(UsersDto.class);
			@SuppressWarnings("unchecked")
			List<UsersDto> list = crit.list();
			System.out.println("Users Size==========="+list.size());
			if(list.size()!=0){
				user = list.get(0);
			}
			System.out.println("process End");
			exchange.getOut().setBody(user);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public void uploadOpencsvFile(Exchange exchange) throws Exception {
		System.out.println("uploadBeanioFile Start"+new Date());
		PartsTemplate template = exchange.getIn().getBody(PartsTemplate.class);
		Response restResp = (Response) exchange.getIn().getHeader("CamelRestletResponse");
		HttpServletResponse httpResp = ServletUtils.getResponse(restResp);
		System.out.println("Headers==="+exchange.getIn().getHeaders().keySet());
		exchange.setProperty("httpResp", httpResp);
		try{
			System.out.println("File Content========="+template.getFileContent());
			CSVUtil csvUtil = new CSVUtil();
			template = csvUtil.getCSVRecords(template);
			template.setFileContent(null);
			exchange.getOut().setBody(template);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
