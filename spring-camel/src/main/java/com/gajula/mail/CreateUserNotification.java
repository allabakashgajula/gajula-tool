package com.gajula.mail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


//import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

//import oracle.core.ojdl.logging.ODLLogger;

import javax.mail.Message;

public class CreateUserNotification {

	
	private static final Logger logger = Logger.getLogger(CreateUserNotification.class.getName());
	  
	  public void sendNotification(String userKey, String EmployeeNumber, String Userlogin, String firstname, String lastname, String fullname, String Email,String Position, String Empsubgroup, String Orgunit,String OrgunitDesc)
	  {
	    try
	    {
	      //String recipients = getMailRecipients(Orgunit);
	      String recipients = "sreenivasareddy.s@accelfrontline.com";
	      System.out.println("Recipients ID: " + recipients);
	      logger.log(Level.INFO, "Recipients ID--: {0}", recipients);
	      String ccrecipients = "g.allabakash@gmail.com";
	      logger.log(Level.INFO, "CCRecipients ID--: {0}", ccrecipients);
	      String body = new String();
	      Properties prop = System.getProperties();
	      prop.setProperty("mail.smtp.host", "smtp.gmail.com");
	      prop.setProperty("mail.smtp.socketFactory.port", "465");
	      prop.setProperty("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
	      prop.setProperty("mail.smtp.auth", "true");
	      prop.setProperty("mail.smtp.port", "465");
	      Session session = Session.getDefaultInstance(prop,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("allabakash.gajula","virtusa@123");
				}
			});
	      MimeMessage message = new MimeMessage(session);
	      
	      //body = "********** This is a system generated email, please do not reply to this mail*************************<br><br> ";
	      body +="<html><head><title>Account Notification mail</title></head>";
	      body +="<body bgcolor='#F0F8FF'><font size=6 color=#556B2F><center>New Email-ID Creation Notification:</center></font><hr/>";
	      body += "Dear All, <br><br> A new emailid has been successfully created for " + fullname+ "(Employee Number: " + EmployeeNumber.substring(2) + ")";
	      body += "<br><br><u>Please find below the details:</u>";
	      body +="<table border=1><tr><td>Employee Number</td><td>Full Name</td><td>User ID</td><td>Email ID</td><td>Position</td><td>OrgUnit</td><td>Orgunit Description</td><td>Employee Subgroup</td></tr>";
	      body +="<tr><td>"+EmployeeNumber.substring(2)+"</td><td>" +fullname+"</td><td>"+Userlogin.toLowerCase()+"</td><td>"+Email.toLowerCase()+"</td><td>"+Position+"</td><td>"+Orgunit+"</td><td>"+OrgunitDesc+"</td><td>"+Empsubgroup+"</td></tr></table>";
	      body += "<br/><br/>Regards,<br/>IDAM Administrator <br/>EmailID:wipro-santosh@tatapower.com <br/>Contact: 022-67172844";
	      body += "<br><br>Please contact <b>IDAM/System Administrators [2844/2870/2875/2876]</b> or <b>HelpDesk Team [2845/2846/2847]</b> in case of any query or clarifications.";
	      body +="<br><br><font color=##990000> ******** This is a system generated email, please do not reply to this email ********</font> ";
		  body +="</body></html>";
	      

	      message.setFrom(new InternetAddress("allabakash.gajula@gmail.com"));
	      

	      InternetAddress[] toaddress = InternetAddress.parse("sreenivasareddy.s@accelfrontline.com");
	      InternetAddress[] ccaddress = InternetAddress.parse(ccrecipients);
	      logger.log(Level.INFO, "Mail recipients--: {0}", recipients);
	      logger.log(Level.INFO, "Mail ccrecipients--: {0}", ccrecipients);
	      message.setRecipients(Message.RecipientType.TO, toaddress);
	      message.setRecipients(Message.RecipientType.CC, ccaddress);
	      message.setSubject("New Email-id Creation of "+fullname+"(Employee Number: " + EmployeeNumber.substring(2) + ")");
	      //message.setSubject("New account has been created for Employee Number: " + EmployeeNumber.substring(2) + " ");
	      message.setContent(body, "text/html; charset=utf-8");
	      message.setHeader("Content-Type", "text/html; charet=utf-8");
	      message.setSentDate(new Date());
	      System.out.println("Mail Body : " + body);
	      logger.info("Notification Message is ready");
	      logger.log(Level.INFO, "Mail Body--: {0}", body);
	      Transport.send(message);
	      logger.info("Notification message has been sent successfully:");
	    }
	    catch (MessagingException e)
	    {
	      e.printStackTrace();
	      logger.info("Exception message:" + e);
	    }
	  }
	  
	  public String getMailRecipients(String orgcode)
	  {
	    String torecipients = new String();
	    Connection con = null;
	    Statement stmt = null;
	    ResultSet rs = null;
	    try
	    {
	      if (orgcode != null)
	      {
	        Properties props = new Properties();
	        props.setProperty("user", "BIZHR");
	        props.setProperty("password", "Tatapower123");
	        String url = "jdbc:oracle:thin:@s0514-oim01.tpc.co.in:1521:OIMDB";
	        Class.forName("oracle.jdbc.driver.OracleDriver");
	        con = DriverManager.getConnection(url, props);
	        stmt = con.createStatement();
	        String query = "select BIZHR from BUSINESSHR where ORGUNIT = '" + orgcode + "'";
	        rs = stmt.executeQuery(query);
	        while (rs.next()) {
	          torecipients = rs.getString("BIZHR");
	        }
	        System.out.println("SQL Query Result : " + torecipients);
	        logger.info("Recipients details sucessfully fetch from DB");
	        logger.log(Level.INFO, "SQL Query Result-- Recipientid=: {0}", torecipients);
	      }
	    }
	    catch (Exception e)
	    {
	      System.out.println("ERROR: In Exception");
	      e.printStackTrace();
	    }
	    return torecipients;
	  }
	  
	  public static void main(String[] args) {
		  CreateUserNotification notification = new CreateUserNotification();
		  notification.sendNotification("allabakash.gajula@gmail.com", 
				                        "8021380",
				                        "allabakash.gajula@gmail.com", 
				                        "allabakash",
				                        "gajula", 
				                        "allabakash gajula",
				                        "allabakash.gajula@gmail.com", 
				                        "consultant",
				                        "agco",
				                        "virtusa",
				                        "virtusa polaris");
	}
}
