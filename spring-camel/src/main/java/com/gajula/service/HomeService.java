package com.gajula.service;

import com.gajula.bean.UsersBean;

public interface HomeService {

	public UsersBean getUsersDetails(String userType)throws Exception;
}
