package com.gajula.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.gajula.bean.UsersBean;
import com.gajula.dao.HomeDao;
import com.gajula.dto.UsersDto;

//@Service
public class HomeServiceImpl implements HomeService{

	@Autowired
	HomeDao homeDao;
	
	public UsersBean getUsersDetails(String userType) throws Exception {
		UsersBean userObj = new UsersBean();
		try{
			System.out.println("Service getUsersDetails Start=="+userType);
			UsersDto user = homeDao.getUsersDetails();
			userObj.setUserId(user.getUserId());
			userObj.setUserName(user.getUserName());
			System.out.println("Service getUsersDetails End");
			return userObj;
		}catch(Exception e){
			e.printStackTrace();
		}
		return userObj;
	}

}
