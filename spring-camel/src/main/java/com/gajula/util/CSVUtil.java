package com.gajula.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

import com.gajula.bean.PartsBean;
import com.gajula.bean.PartsTemplate;

public class CSVUtil {

	private static String IMPORT_TEMP_DIR=System.getProperty("catalina.base") + "/temp/";
	private static String[] CSV_HEADERS = new String[] {"Part Number","Factory Code","Qty","Price"};
	
	public PartsTemplate getCSVRecords(PartsTemplate template)throws Exception{
		try{
			template = CSVUtil.getParseCSVToBeanList(template);
			if(getCsvHeadersValidate(CSV_HEADERS,template.getPartsRecords().get(0))){
				template.setPartsRecords(null);
				template.setFileContent(null);
				template.setErrorMessageFlag("T");
				template.setErrorMessage("Invalid Import File Headers");
			}
			return template;
		}catch(Exception e){
			e.printStackTrace();
		}
		return template;
	}
	
	public boolean getCsvHeadersValidate(String[] headers,PartsBean csvHeader)throws Exception{
		boolean headersErrFlag = false;
		try{
			System.out.println("getCsvHeadersValidate Start-------------");
			if(!headers[0].equalsIgnoreCase(csvHeader.getPartNumber())){ headersErrFlag = true; }
			if(!headers[1].equalsIgnoreCase(csvHeader.getFactoryCode())){ headersErrFlag = true; }
			if(!headers[2].equalsIgnoreCase(csvHeader.getQty())){ headersErrFlag = true; }
			if(!headers[3].equalsIgnoreCase(csvHeader.getPrice())){ headersErrFlag = true; }
			System.out.println("getCsvHeadersValidate End-----------");
			return headersErrFlag;
		}catch(Exception e){
			headersErrFlag = false;
			System.out.println("getCsvHeadersValidate Error--"+e.getMessage());
			return headersErrFlag;
		}
	}
	
	public static PartsTemplate getParseCSVToBeanList(PartsTemplate template)throws Exception{
		List<PartsBean> pcaParts = new ArrayList<PartsBean>();
		PartsBean csvBean = new PartsBean();
		FileOutputStream fos = null;
		String fileLocation = null;
		String[] headers = null;
		File file = null;
		try{
			System.out.println("getParseCSVToBeanList Start---------");
			long timestamp = new Date().getTime();
			String fileName = "CSVImport.csv";
			fileLocation = IMPORT_TEMP_DIR+fileName;
			System.out.println("CSV PATH=========="+fileLocation);
			CsvToBean<PartsBean> csvToBean = new CsvToBean<PartsBean>();
	        ColumnPositionMappingStrategy<PartsBean> strat = new ColumnPositionMappingStrategy();
	        strat.setType(PartsBean.class);
	        String[] columns = new String[] {"Part Number","Factory Code","Qty","Price"}; 
	        strat.setColumnMapping(columns);
	        file = new File(fileLocation);
			if(!file.exists()){
				file.createNewFile();
			}
			fos = new FileOutputStream(file);
			fos.write(template.getFileContent().getBytes());
			CSVReader reader = new CSVReader(new FileReader(file));
	        List<String[]> records = reader.readAll();
	        if(records.size()!=0){
	        	headers = records.get(0);
	        	System.out.println("HEADERS======"+Arrays.toString(headers));
	        	for(String[] record:records){
	        		System.out.println("RECORD----"+Arrays.toString(record));
	        		csvBean = new PartsBean();
	        		//System.out.println("record--------"+record[0]+"::"+record[1]+"::"+record[2]+"::"+record[3]);
	        		csvBean.setPartNumber(record[0]);
	        		csvBean.setFactoryCode(record[1]);
	        		csvBean.setQty(record[2]);
	        		csvBean.setPrice(record[3]);
	        		pcaParts.add(csvBean);
	        	}
	        }
	        reader.close();
	        fos.flush();
			fos.close();
	        System.out.println("CSV INPUT PARTS LIST Size:::"+pcaParts.size());
			System.out.println("getParseCSVToBeanList End--");
			template.setPartsRecords(pcaParts);
			template.setErrorMessageFlag("F");
			template.setErrorMessage("");
        	System.out.println("getParseCSVToBeanList End----------");
			return template;
		}catch(Exception e){
			template.setPartsRecords(null);
			template.setFileContent(null);
			template.setErrorMessageFlag("T");
			template.setErrorMessage("Invalid CSV File");
        	System.out.println("getParseCSVToBeanList Error---"+e.getMessage());
			return template;
		}finally{
			if (fos != null) try { fos.close(); file.delete(); } catch (Exception e) {}
		}
		
	}
	
}
