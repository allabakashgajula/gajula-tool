package com.gajula.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gajula.bean.UsersBean;

@Controller
public class MenuOptionController {

	@RequestMapping(value = { "/opencsv"}, method = RequestMethod.GET)
	public String getOpenCSV(ModelMap model) {
		System.out.println("OPENCSV Content");
		String message = "OPENCSV @ Gajula";
		model.addAttribute("message",message); 
        return "opencsv";  
	}
	
	@RequestMapping(value = { "/beanio"}, method = RequestMethod.GET)
	public String getBeanIO(ModelMap model) {
		System.out.println("BEAN-IO Content");
		String message = "BEAN-IO @ Gajula";
		model.addAttribute("message",message); 
        return "beanio";  
	}
	
	@RequestMapping(value = { "/userForm"}, method = RequestMethod.GET)
	public String userForm(ModelMap model) {
		String message = "MAIN @ Gajula";
		model.addAttribute("message",message);
		UsersBean user = new UsersBean();
		model.addAttribute("userForm", user);
        return "userForm";  
	}
	
	@RequestMapping(value = { "/socialAuntheticate"}, method = RequestMethod.GET)
	public String socialAuntheticate(ModelMap model) {
		String message = "socialAuntheticate @ Gajula";
		model.addAttribute("message",message);
		return "socialAuntheticate";  
	}
	
	@RequestMapping(value = { "/dropbox"}, method = RequestMethod.GET)
	public String getDropbox(ModelMap model) {
		String message = "Dropbox @ Gajula";
		model.addAttribute("message",message);
		return "dropbox";  
	}
}
