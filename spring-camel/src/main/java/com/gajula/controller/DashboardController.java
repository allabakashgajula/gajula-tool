package com.gajula.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DashboardController {

	@RequestMapping(value = { "/dashboard"}, method = RequestMethod.GET)
	public String homePage(ModelMap model) {
		String message = "Hello World, Spring MVC @ Gajula";
		model.addAttribute("message",message); 
        return "dashboard";  
	}
}
