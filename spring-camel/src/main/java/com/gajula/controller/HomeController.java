package com.gajula.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.gajula.bean.UsersBean;
import com.gajula.validator.UserFormValidator;

@Controller
public class HomeController {

	/*@Autowired
	HomeService homeService;*/
	
	@Autowired
	UserFormValidator userFormValidator;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(userFormValidator);
	}
	
	@RequestMapping(value="home",method=RequestMethod.POST)
	public ModelAndView getHomeResponse(@ModelAttribute("userForm")UsersBean user,HttpServletRequest req,
			HttpServletResponse res, SessionStatus status,
			BindingResult result, Model model)throws Exception{
		System.out.println("getHomeResponse Start");
		System.out.println("User name==="+user.getUserName());
		userFormValidator.validate(user, result);
		if(result.hasErrors()){
			System.out.println("Error");
			ModelAndView mav = new ModelAndView("Home");
			mav.addObject("user", user);
			return mav;
		}else{
			if(user.getUserName().equalsIgnoreCase("Admin")){
				System.out.println("Admin");
				//homeService.getUsersDetails(userType);
				ModelAndView mav = new ModelAndView("Admin");
				mav.addObject("userForm", user);
				return mav;
			}else if(user.getUserName().equalsIgnoreCase("Guest")){
				System.out.println("Guest");
				//homeService.getUsersDetails(userType);
				ModelAndView mav = new ModelAndView("Guest");
				mav.addObject("userForm", user);
				return mav;
			}
		}
		System.out.println("getHomeResponse End");
		ModelAndView mav = new ModelAndView("Home");
		mav.addObject("user", user);
		return mav;
	}
	
	
	
}
