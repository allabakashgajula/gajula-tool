package com.gajula.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gajula.ioc.PrototypeScope;
import com.gajula.ioc.SingletonScope;

public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		SingletonScope sinScope = (SingletonScope)context.getBean("sinScope");
		sinScope.getMsg();
		
		System.out.println("---------------Before--------------");
		System.out.println(sinScope.getPrototypeScope().getId());
		System.out.println(sinScope.getPrototypeScope().getName());
		
		PrototypeScope prototypeBean = (PrototypeScope)context.getBean("prototypeScope");
		prototypeBean.setId("1102");
		prototypeBean.setName("Bakash");
		System.out.println("---------After Protype Update-----------");
		System.out.println("In Prototype--------"+prototypeBean.getId()+"==="+prototypeBean.getName());
		
		System.out.println("In Singleton Old Instance-----------"+sinScope.getPrototypeScope().getId()+"===="+sinScope.getPrototypeScope().getName());
		SingletonScope sinScope1 = (SingletonScope)context.getBean("sinScope");
		System.out.println("In Singleton New Instance-----------"+sinScope1.getPrototypeScope().getId()+"===="+sinScope1.getPrototypeScope().getName());
		
		
		SingletonScope sinScope2 = (SingletonScope)context.getBean("sinScope");
		sinScope2.getPrototypeScope().setId("1103");
		sinScope2.getPrototypeScope().setName("Virtusa");
		
		System.out.println("---------After Static Update-----------");
		System.out.println("In Singleton-----------"+sinScope.getPrototypeScope().getId()+"===="+sinScope.getPrototypeScope().getName());
	}

}
