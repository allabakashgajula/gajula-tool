package com.gajula.ioc;

public class SingletonScope {

	private PrototypeScope prototypeScope;
	
	public SingletonScope(){
		System.out.println("Constructor SingletonScope");
	}
	
	public void getMsg(){
		System.out.println("Get Message");
	}

	public PrototypeScope getPrototypeScope() {
		return prototypeScope;
	}

	public void setPrototypeScope(PrototypeScope prototypeScope) {
		this.prototypeScope = prototypeScope;
	}

	
	
	
}
