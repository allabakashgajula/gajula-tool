package com.gajula.ioc;

public class PrototypeScope {
	
	public PrototypeScope(){
		System.out.println("Constructor PrototypeBean");
	}
	
	private String id;
	private String name;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
}
