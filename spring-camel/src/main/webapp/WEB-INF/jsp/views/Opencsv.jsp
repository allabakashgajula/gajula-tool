<%@ page session="false"%>
<%@ taglib prefix="bean" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%-- <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> --%>

<%@ page language="java" contentType="text/html; charset=utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="<bean:url value="resource/bower/jquery/dist/jquery-1.11.3.js"/>"  type="text/javascript"></script>
<script src="<bean:url value="resource/scripts/app.js"/>" type="text/javascript"></script>
<script src="<bean:url value="resource/scripts/socialAuthentication.js"/>" type="text/javascript"></script>

<style type="text/css">
.container {
  display: flex;
  flex-direction: column;
}
.container > div {
  padding: 20px;
  color: #fff;
  margin: 5px;
  text-align: center;
  font-size: 30px;
}
.cyanDiv {
  background: #ff5423;
}

.redDiv {
  background: #049500;
}

</style>

</head>
<body>

<div class="container">

	<div class="cyanDiv">
	<table align="left" border="0" bordercolor="black">
		<tr>
			<td colspan="2" style="text-align:center;font-weight:bold;">Open CSV</td>
		</tr>
		<tr>
			<td>
			<form action="#" method="post" id="beanioForm" name="beanioForm">
			<td><input type="file" id="opencsvFile" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" style="cursor:pointer;"/>
			<input type="button" value="Import" id="sendOpencsvfile"></td>
			</form>
			</td>
		</tr>
	</table>
	</div>

	<div class="redDiv">
		<div id="opencsvResult"></div>
	</div>
</div>

</body>
</html>