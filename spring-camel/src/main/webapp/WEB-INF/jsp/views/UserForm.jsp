<%@ page session="false"%>
<%@ taglib prefix="bean" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%-- <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> --%>

<%@ page language="java" contentType="text/html; charset=utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="<bean:url value="resource/bower/jquery/dist/jquery-1.11.3.js"/>"  type="text/javascript"></script>
<script src="<bean:url value="resource/scripts/app.js"/>" type="text/javascript"></script>
<script src="<bean:url value="resource/scripts/socialAuthentication.js"/>" type="text/javascript"></script>
</head>
<body>

<div align="left">

<h3><a href="#" id="isUserExists">Get User Details</a></h3>

<%-- <h2><bean:message code="usersFormHeader" /></h2> --%>
<form:form method="post" modelAttribute="userForm" action="userForm.htm">
	<table border="1" background="cyan">
	<tr>
	<td colspan="2"><bean:message code="label.userName" />
	<td><form:input path="userName" type="text" />
	<span style="color:red;">*<form:errors path="userName" /></span></td>
	</tr>

	<tr>
	<td colspan="2"><bean:message code="label.firstName" />
	<td><form:input path="firstName" type="text" />
	<span style="color:red;">*<form:errors path="firstName" /></span></td>
	</tr>
	
	<tr>
	<td colspan="2"><bean:message code="label.lastName" />
	<td><form:input path="lastName" type="text" />
	<span style="color:red;">*<form:errors path="lastName" /></span></td>
	</tr>
	
	<tr>
	<td colspan="2"><bean:message code="label.gender" />
	<td><form:input path="gender" type="text" />
	<span style="color:red;">*<form:errors path="gender" /></span></td>
	</tr>
	
	<tr>
	<td colspan="2"><bean:message code="label.mobile" />
	<td><form:input path="mobile" type="text" />
	<span style="color:red;">*<form:errors path="mobile" /></span></td>
	</tr>
	
	<tr>
	<td colspan="2"><bean:message code="label.address1" />
	<td><form:input path="address1" type="text" />
	<span style="color:red;">*<form:errors path="address1" /></span></td>
	</tr>
	
	<tr>
	<td colspan="2"><bean:message code="label.address2" />
	<td><form:input path="address2" type="text" />
	<span style="color:red;">*<form:errors path="address2" /></span></td>
	</tr>
	
	<tr>
	<td colspan="2" align="center"><input type="submit" name="Submit" value="Submit" /></td>
	<td></td>
	</tr>
	</table>
	<input>	
</form:form>
</div>
<div id="springResult">
<h1>Results</h1>
</div>

</body>
</html>