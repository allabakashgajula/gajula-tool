<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
<title>Header</title>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="<c:url value='/resources/css/bootstrap.css' />"  rel="stylesheet"></link>
</head>
</head>
<body>
<div style="align:left;border:1;border-color:black;background-color:#66CC00;font-size:25px;color:white">
<h2 align="center">Spring Camel Dashboard</h2><hr/>

<a style="color:white" href="dashboard.htm">Dashboard</a> |   
<a style="color:white" href="contact.htm">Contact Us</a>

<bean:message code="chooseLanguage" /> : 
	<select id="chooseLanguage">
		<option value="en">Choose</option>
		<option value="en_US">English(US)</option>
		<option value="hi_IN">Hindi</option>
	</select>

</div>  

<div id="userProfile" align="right">
<table>
<tr>
	<td rowspan="2"><img alt="left" src="ViewResource/images/userIcon.jpg" id="usrImg" height="50" width="50"></td>
	<td><div id="givenName"><span></span></div></td>
</tr>
<tr><td><div id="email"><span></span></div></td></tr>
</table>
</div>

</body>