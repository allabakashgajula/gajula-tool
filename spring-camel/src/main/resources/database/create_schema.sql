USE [gajula_dev]
GO
CREATE TABLE [dbo].[product](
	[productId] [varchar](10) PRIMARY KEY,
	[productName] [varchar](50) NULL
);

CREATE TABLE [dbo].[User](
	[UserId] [varchar](30) PRIMARY KEY,
	[RoleId] [bigint] NOT NULL,
	[LastName] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[Status] [varchar](10) NOT NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [varchar](100) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL
);


INSERT [dbo].[product] ([productId], [productName]) VALUES (N'4401', N'Java')
INSERT [dbo].[product] ([productId], [productName]) VALUES (N'4402', N'Camel')
INSERT [dbo].[product] ([productId], [productName]) VALUES (N'4403', N'Angular')

INSERT into [gajula_dev].[dbo].[User] 
([UserId], [RoleId], [LastName], [FirstName], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) 
VALUES ('gajula', 1, 'Gajula', 'Allabakash', 'Active', 'gajula',GETDATE(),'gajula',GETDATE() )

INSERT into [gajula_dev].[dbo].[User] 
([UserId], [RoleId], [LastName], [FirstName], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) 
VALUES ('saif', 2, 'Khaja', 'Saifuddin', 'Active', 'gajula',GETDATE(),'gajula',GETDATE() )


select * from product;
select * from [User];

